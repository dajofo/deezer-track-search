import { combineReducers } from 'redux';
import TracksReducer from './reducer-tracks';

const rootReducer = combineReducers({
  tracks: TracksReducer
});

export default rootReducer;