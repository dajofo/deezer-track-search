import {
	IS_FETCHING,
	FETCHING_ERROR, 
	FETCH_NEW_TRACKS, 
	FETCH_MORE_TRACKS,
	SET_SORT_PREF,
	SET_SEARCH_QUERY } from '../actions';

const initialState = {
  searchQuery: '',
  trackList: [],
  nextURL: null,
  isFetching: false,
  hasFetched: false,
  totalTracks: 0,
  sortPref: null,
  hasError: false
}

const tracksReducer = (state = initialState, action) => {
  switch (action.type) {
  	case SET_SEARCH_QUERY:
  		return {
  			...state, 
  			searchQuery: action.payload,
  			hasFetched: false,
  		};

    case IS_FETCHING:
      return { ...state, isFetching: action.payload };
    
    case FETCH_NEW_TRACKS:
      return {
        ...state,
        trackList: action.payload.data,
        nextURL: action.payload.next,
        totalTracks: action.payload.total,
        isFetching: false,
        hasFetched: true,
        hasError: action.payload.error ? true : false
      };

    case FETCH_MORE_TRACKS:
    	return { 
    		...state, 
    		trackList: state.trackList.concat(action.payload.data),
        isFetching: false,
    		nextURL: action.payload.next
    	}

    case SET_SORT_PREF:
    	return { 
    		...state, 
    		sortPref: action.payload,
    		hasFetched: false,
    	}

    case FETCHING_ERROR:
    	return { ...state,  hasError: action.payload }
    
    default:
      return state;
  }
}

export default tracksReducer;