import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';

// CONTAINERS
import Search from './components/search/container';
import TrackList from './components/track-list';

import './app.css';

const store = applyMiddleware(thunk)(createStore)(reducers);

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <div className="deezer-app-container">
          <header className="site-header-container">
            <h1 className="site-title">Deezer Track Search</h1>
          </header>
          <div className="search-container">
          	<Search />
          </div>
          <TrackList />
        </div>
      </Provider>
    );
  }
}

export default App;
