import { connect } from 'react-redux';
import Search from './index';
import { changeSearchQuery } from '../../actions';

const mapStateToProps = state => ({
  searchQuery: state.searchQuery
});

const mapDispatchToProps = dispatch => ({
  onChange: value => dispatch(changeSearchQuery(value))
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
