import React from 'react';
import PropTypes from 'prop-types';

const Search = ({ searchQuery, onChange }) =>
  <input
    className="input-search"
    type="text"
    placeholder="Find some tunes"
    value={ searchQuery }
    onChange={ event => onChange(event.target.value) }
  />

Search.propTypes = {
  searchQuery: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

export default Search;