import React from 'react';
import PropTypes from 'prop-types';

const Track = ({ photoUrl, photoAlt, artistName, trackTitle }) =>
  <div className="cf data-grid__row data-grid__row">
    <div className="data-grid__cell data-grid__cell--image">
      <img src={ photoUrl } alt={ photoAlt } />
    </div>
    <div className="data-grid__cell data-grid__cell--artist">{ artistName }</div>
    <div className="data-grid__cell data-grid__cell--track">{ trackTitle }</div>
  </div>

Track.propTypes = {
  photoUrl: PropTypes.string.isRequired,
  photoAlt: PropTypes.string.isRequired,
  artistName: PropTypes.string.isRequired,
  trackTitle: PropTypes.string.isRequired
};

export default Track;