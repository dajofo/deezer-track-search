import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchMoreTracks, changeSortPref } from '../../actions';
import Track from '../track';
import Loader from '../loader';

const scrollOffset = 50; // px from bottom of page to trigger loadMoreTracks

class TrackList extends Component {
	componentWillMount = (props) => window.addEventListener('scroll', this.checkScroll);

	checkScroll = () => {
    let scrollEl = document.scrollingElement || document.documentElement;
    let scrollDepth = scrollEl.scrollTop;
		let windowBottom = window.innerHeight + scrollDepth;
		if ( (windowBottom + scrollOffset) >= document.body.clientHeight ) {
			this.loadMoreTracks();
		}
	}

  loadMoreTracks() {
    const {
      nextURL,
      isFetching,
      fetchMoreTracks
    } = this.props;

  	if (nextURL && isFetching === false) {
  		fetchMoreTracks(nextURL);
  	}
  }

  sortGrid = event => {
    const { changeSortPref } = this.props;

  	let newSortPref = event.target.getAttribute('data-sort');
		changeSortPref(newSortPref);
  }

  renderGrid() {
    const { sortPref } = this.props;
    let artistSortClass = sortPref === 'ARTIST_ASC' ? ' sorting-asc' : sortPref === 'ARTIST_DESC' ? ' sorting-desc' : '';
    let trackSortClass = sortPref === 'TRACK_ASC' ? ' sorting-asc' : sortPref === 'TRACK_DESC' ? ' sorting-desc' : '';

  	return (
  		<div className="data-grid">
  			<div className="cf data-grid__row data-grid__row--header">
  				<div 	className="data-grid__cell data-grid__cell--header data-grid__cell--image"></div>
  				<div 	className={ `data-grid__cell data-grid__cell--header data-grid__cell--artist${artistSortClass}` }
  							data-sort={ sortPref !== 'ARTIST_ASC' ? 'ARTIST_ASC' : 'ARTIST_DESC' } 
  							onClick={ this.sortGrid }>
  							Artist
  				</div>
  				<div 	className={ `data-grid__cell data-grid__cell--header data-grid__cell--track${trackSortClass}` }
  							data-sort={ sortPref !== 'TRACK_ASC' ? 'TRACK_ASC' : 'TRACK_DESC' }
  							onClick={ this.sortGrid }>
  							Track
					</div>
  			</div>

  			{ this.renderRows() }
  		</div>
  	);
  }

  renderRows() {
    const { trackList } = this.props;

    return [...trackList].map((track, key) => {
      const trackData = {
        photoUrl: track.artist.picture_small,
        photoAlt: `Artist photo of ${track.artist.name}`,
        artistName: track.artist.name,
        trackTitle: track.title
      };

      return <Track key={ key } { ...trackData } />;
    });
  }

  render() {
    const {
      hasError,
      isFetching,
      hasFetched,
      totalTracks,
      nextURL
    } = this.props;

  	if ( hasError || (!isFetching && !hasFetched) ) {
  		return null;
  	}

  	if (isFetching && !hasFetched) {
  		return <Loader />;
  	}

  	if (hasFetched && totalTracks === 0) {
  		return <div className="msg-no-results">Dang, no results for that.</div>;
  	}

    return (
    	<div className="track-list-container">
    		{ this.renderGrid() }
    		{ nextURL ? <Loader /> : <div className="total-tracks">{ totalTracks } { totalTracks === 1 ? 'track' : 'tracks' }</div> }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.tracks
});

export default connect(mapStateToProps, { fetchMoreTracks, changeSortPref })(TrackList);
