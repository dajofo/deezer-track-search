import axios from 'axios';

// Used a proxy to get around the CORS restrictions for working locally
const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';
const ROOT_URL = 'http://api.deezer.com/search/track?q='

// --------------------------------------------------------------------

export const IS_FETCHING = 'IS_FETCHING';
export const tracksLoading = (bool) => ({ 
  	type: IS_FETCHING,
  	payload: bool 
  });

// --------------------------------------------------------------------

export const FETCHING_ERROR = 'FETCHING_ERROR';
export const fetchingError = (bool) => ({ 
  	type: FETCHING_ERROR,
  	payload: bool 
  });

// --------------------------------------------------------------------

export const CHANGE_SORT_PREF = 'CHANGE_SORT_PREF';
export const changeSortPref = (sortPref) => {
	return (dispatch, getState) => {
		dispatch( setSortPref(sortPref) );

		let query = getState().tracks.searchQuery;
		dispatch( fetchNewTracks(query, sortPref) );
	};
}

// --------------------------------------------------------------------

export const SET_SORT_PREF = 'SET_SORT_PREF';
export const setSortPref = (pref) => ({ 
  	type: SET_SORT_PREF,
  	payload: pref 
  });

// --------------------------------------------------------------------

export const CHANGE_SEARCH_QUERY = 'CHANGE_SEARCH_QUERY';
export const changeSearchQuery = (query) => {
	return (dispatch, getState) => {
		dispatch( setSearchQuery(query) );

		let sortPref = getState().tracks.sortPref;
		dispatch( fetchNewTracks(query, sortPref) );
	};
}

// --------------------------------------------------------------------

export const SET_SEARCH_QUERY = 'SET_SEARCH_QUERY';
export const setSearchQuery = (query) => ({ 
  	type: SET_SEARCH_QUERY,
  	payload: query 
  });

// --------------------------------------------------------------------

export const FETCH_NEW_TRACKS = 'FETCH_NEW_TRACKS';
export const fetchNewTracks = (query, sortPref = 'RANKING') => {
	let url = `${PROXY_URL}${ROOT_URL}${query}&order=${sortPref}&index=0`;
  const request = axios.get(url);

  return (dispatch) => {
  	dispatch( tracksLoading(true) );
    request.then(({data}) => {
      dispatch({ type: FETCH_NEW_TRACKS, payload: data });
    }).catch((err) => {
    	dispatch( fetchingError(true) );
    });
  };
}

// --------------------------------------------------------------------

export const FETCH_MORE_TRACKS = 'FETCH_MORE_TRACKS';
export const fetchMoreTracks = url => {
	const request = axios.get(`${PROXY_URL}${url}`);

	return (dispatch) => {
  	dispatch( tracksLoading(true) );
    request.then(({data}) => {
      dispatch({ type: FETCH_MORE_TRACKS, payload: data });
    });
  };
}
