## Deezer Track Search

######Author: David Forsythe

A React/Redux app for searching Deezer tracks.  

## Features Include: 
* Instant search as the user types.

* Results display in a data grid, showing artist photo, artist name, and track name.

* Loading indicator.

* Infinite scroll: when user scrolls near the bottom of the page, more results are fetched.

* Once all tracks are fetched, the total results count is shown below the data grid.

* If no results are found, a sad message is shown. ( ಥ_ಥ )

* The grid is alphabetically sortable by artist or track name by clicking on the corresponding grid header.

* Minimal error handling

* Responsive design.


## Installation

    git clone https://dajofo@bitbucket.org/dajofo/deezer-track-search.git

    nvm use

    yarn install

    yarn start